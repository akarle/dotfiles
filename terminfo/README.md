# Terminfo Dotfiles

At the time of adding, the sole purpose of these files is to add italics support to iTerm2, tmux, and vim.

For the setup instructions, see this [link](https://alexpearce.me/2014/05/italics-in-iterm2-vim-tmux/).

Also check out [this link](https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be) for the tmux parts that got it working for me.
