# dotfiles
My dotfiles.

## Basic Setup
1. My vimrc is tailored for Vim 8
2. Set up Vim-Plug for packages [instructions here](https://github.com/junegunn/vim-plug). Note, the vimrc should be good to go for no packages as well (for a quick and minimal install)
3. Run :PlugInstall, and you should be good to go!

## Install

Note that bash is required for this install (as some of the interactive options depend on bash).

```
bash -c "$(curl -fsSL https://raw.githubusercontent.com/akarle/dotfiles/master/install.sh)"
```
