" .vimrc by Alex Karle
" Hosted at https://github.com/akarle/dotfiles
"
" Inspired by tips all over the interwebs
" For best results install vim-plug:
"   https://github.com/june-gunn/vim-plug

" GENERAL SETTINGS {{{
set number              " Line numbers on

" Space settings
set softtabstop=4       " <TAB> is 4 spaces (when expandtab set)
set shiftwidth=4        " >> will indent 4 spaces
set expandtab           " Needed to replace <TAB> with spaces
set autoindent          " Indent carries over on new line
set smarttab            " Use shiftwidth for <TAB> and <BS>

"backspace should function as expected
set backspace=indent,eol,start

"search highlighting and incremental search
set hlsearch
set incsearch

" detect filetype + load plugins + indent accordingly
filetype plugin indent on

" Link unnamed register to system clipboard
" (effectively d, x, y, etc all write to system clipboard)
set clipboard=unnamed

" Disable  modelines for security (modeline vulnerability)
set modelines=0

" Encoding
set encoding=utf-8

" Show whitespace with 'set: list' (disable: set: nolist)
set listchars=space:·,tab:>–,trail:~,eol:¬

" Prevent {}, (), [] commands from opening folds
set foldopen-=block

" Prevent windows resizing when split opened/closed
set noequalalways

" Show the command being typed
set showcmd
set history=1000

" More visual completion for command mode
set wildmenu

" Set key code timeout to be less to avoid <ESC> delay
set ttimeout
set ttimeoutlen=30

" Scroll when cursor gets within 5 rows of screen end
set scrolloff=5

" Persistent undo
if(has('persistent_undo') && !empty(finddir('~/.vim/undo', '~')))
    set undodir=~/.vim/undo/
    set undofile
    set undolevels=1000
    set undoreload=10000
endif

" Backup files to ~/.vim/swp if present, else .
set directory=~/.vim/swp,.

" Faster grepping!
if executable('rg')
    set grepprg=rg\ --vimgrep\ --no-heading
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif
" }}}

" PLUGIN RELATED SETTINGS {{{
" Only load plugins if vim-plug installed!
if filereadable(expand("~/.vim/autoload/plug.vim"))
    " INSTALL PLUGINS {{{
    " Plugs will be downloaded under the specified directory.
    call plug#begin('~/.vim/bundle')

    " Colorscheme
    Plug 'morhetz/gruvbox'

    " Extra syntax files
    Plug 'octol/vim-cpp-enhanced-highlight'
    Plug 'pangloss/vim-javascript'
    Plug 'posva/vim-vue'

    " Plug nerdtree for tree package explorer
    Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

    " Airline = status bar for vim
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    " Fugitive = git for vim
    Plug 'tpope/vim-fugitive'

    " NERD Commenter = for comments
    Plug 'scrooloose/nerdcommenter'

    " Git Gutter
    Plug 'airblade/vim-gitgutter'

    " Syntastic for syntax checks
    "Plug 'vim-syntastic/syntastic', { 'for': 'python' }

    " todo task plugin
    Plug 'irrationalistic/vim-tasks'

    " Simple latex tools
    Plug 'lervag/vimtex', { 'for': 'tex' }

    " fzf -- fuzzy finder (installed system wide)
    Plug 'junegunn/fzf', { 'dir': '~/.fzf' }

    " surround for parens and such
    Plug 'tpope/vim-surround'

    " markdown rendering
    Plug 'suan/vim-instant-markdown'

    " code-completion (leave commented usually -- slows down start)
    " Plug 'Valloric/YouCompleteMe'

    " List ends here. Plugins become visible to Vim after this call.
    call plug#end()
    " }}}

    " PLUGIN SETTINGS {{{
    " Syntastic Default Settings
    set statusline+=%#warningmsg#
    "set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*

    "let g:syntastic_always_populate_loc_list = 1
    "let g:syntastic_auto_loc_list = 1
    "let g:syntastic_check_on_open = 0
    "let g:syntastic_check_on_wq = 0

    "let g:syntastic_python_checkers = ['flake8']
    "let g:syntastic_python_flake8_post_args='--ignore=E116'

    " vimtex stuff
    " Set pdf viewer to sim
    let g:vimtex_view_method='skim'

    "for airline status bar -- need to include font
    let g:airline_powerline_fonts = 0

    " don't open instant markown on open md
    let g:instant_markdown_autostart = 0
    " }}}
endif
" }}}

" THEME {{{
" When in MacVim don't show the scrollbars
set guioptions-=r
set guioptions-=L

"Syntax highlighting on
syntax on

" auto commands (grouped so that resourcing doesn't pile up)
if(has('autocmd'))
    augroup MyCustomAucmds
        " removes all autocmds from this group (needed when re-sourcing)
        autocmd!

        " Set cursor line when in insert mode (to indicate insert mode)
        autocmd InsertEnter,InsertLeave * set cul!
        autocmd BufNewFile,BufRead *.notes set filetype=markdown
    augroup END  " goes back to default augroup
endif

" COLORSCHEME
" if can use truecolor, do
if (has("termguicolors"))
    " load colorscheme if plugins installed
    if filereadable(expand("~/.vim/bundle/gruvbox/colors/gruvbox.vim"))
        let g:gruvbox_italic = 1
        set termguicolors
        colorscheme gruvbox
        " make pythonSelf red not grey
        highlight link pythonSelf GruvboxBlue
    endif

    " Needed for termgui in tmux--&term begins w screen-...
    if &term =~# '^tmux'
        " Set the terminal foreground+background colors
        " See :h xterm-true-color
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif
endif

set background=dark  " seems to need to be after gruvbox


" Allow cursor to change shape
" https://stackoverflow.com/questions/6488683/how-do-i-change-the-vim-cursor-in-insert-normal-mode/42118416#42118416
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
" }}}

" FUNCTIONS {{{
" A function to execute a command and return to the old position
" CREDIT: http://vimcasts.org/episodes/tidying-whitespace/
function! CMDPreserve(command) abort
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  execute a:command
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

function! ToggleColorColumn() abort
    if &colorcolumn == ""
        set colorcolumn=80
    else
        set colorcolumn=""
    endif
endfunction
" }}}

" KEY MAPPINGS {{{
" set leader to be spacebar
let mapleader = " "

"Map ctrl-i/j/k/h to switch between splits
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

if(has('terminal'))
    tnoremap <C-j> <C-w>j
    tnoremap <C-k> <C-w>k
    tnoremap <C-h> <C-w>h
    tnoremap <C-l> <C-w>l
    tnoremap <C-w>t <C-w>:tabnext<CR>
endif

"Map ctrl-p to toggle paste mode
nnoremap <C-p> :set paste!<CR>

" Elim Whitespace (through regexp)
nnoremap <leader><space> :call CMDPreserve("%s/\\s\\+$//e")<CR>

" save session
" nnoremap <leader>s :mksession!<CR>

" edit in current buffer dir
nnoremap <leader>e :e %:h/
nnoremap <leader>v :vsp %:h/
nnoremap <leader>s :sp %:h/

" quick toggle for light/dark background
nnoremap <leader>L :set background=light<CR>
nnoremap <leader>D :set background=dark<CR>

" toggle color column (to ensure short lines)
nnoremap <leader>C :call ToggleColorColumn()<CR>

" easier tab switching (good for use w terminal wher gt doesn't work)
nnoremap <C-w>t :tabnext<CR>

" call fzf!
nnoremap <leader>f :FZF<CR>
" }}}
