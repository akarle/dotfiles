" ignore the wrapped lines
noremap  <buffer> <silent> k gk
noremap  <buffer> <silent> j gj
noremap  <buffer> <silent> 0 g0
noremap  <buffer> <silent> $ g$

" spell check!
setlocal spell spelllang=en_us

" soft word wrap should not break words
" NOTE: don't use listchars with this feature
set linebreak
