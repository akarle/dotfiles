" wrap at word breaks
setlocal linebreak

" have broken lines indent!
setlocal breakindent

" spell check
set spell
