" for neovim development!
if exists( "g:loaded_youcompleteme" )
    nnoremap <buffer> <c-]> :YcmCompleter GoTo<CR>
endif
